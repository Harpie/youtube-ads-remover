Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
}

function findTime(){
    if(!window.ytplayer || !window.ytplayer.config)
        return;
    var orginalTime = window.ytplayer.config.args.length_seconds;
    var sec = orginalTime % 60;
    var min = Math.floor(orginalTime /60 );
    var ho = Math.floor(min /60 );
    min %= 60;
    var ti = ""+ (ho ? (ho + ":") : "") + min + ":";
    return [ti + (sec - 1).pad(2), ti + (sec).pad(2), ti + (++sec).pad(2)];
}

var removeAdsCounter;

function removeAdsOrReload(){
    if(removeAdsCounter > 0)
        return;
    removeAdsCounter = 0;
    removeAdsOrReloadR();
}

function removeAdsOrReloadR(){
    if(removeAdsCounter++ > 15)
        location.reload(false);
    var videoLength = document.querySelector(".ytp-time-duration").textContent;
    if (!videoLength)
        setTimeout(removeAdsOrReloadR, 750);
    
    //console.log(videoLength);
    //console.log(posibleVideoLengths);
    
    // if vide length other then defined in yt config
    if(!posibleVideoLengths.includes(videoLength)) {
        localStorage.setItem("reload", "true");
        // broke ad
        document.querySelector(".video-stream").setAttribute("src", "");
        // wait till yt think you haved watch ad
        setTimeout(removeAdsOrReloadR, 750);
    } else {
        removeAdsCounter = 0;
        localStorage.setItem("reload", "");
    }
}

var posibleVideoLengths;

function rm(){
    // remove dom ads classes
    document.querySelectorAll("[class^='ad-'],[class*=' ad-'],[class*='-ad-'],[class*='-ads ']").forEach(function(el) {
      if(typeof(el.className) === 'string' ) {
        const classes = el.className.split(" ").filter(c => !c.startsWith("ad-") && !c.includes('-ad-') && !c.endsWith('-ads'));
        el.className = classes.join(" ").trim();
      }
    });
    // remove ads only elements
    document.querySelectorAll("[id*='-ad'],[id^='ad-']").forEach(e => e.parentNode.removeChild(e));

    
    if(!posibleVideoLengths)
        posibleVideoLengths = findTime();
    else
        removeAdsOrReload();
}

// continue play after reload
if(localStorage.getItem("reload") == "true"){
    localStorage.setItem("reload", "");
    var pla = document.getElementsByClassName('ytp-play-button');
    if(pla.length > 0)
        pla[0].click();
}

rm();
setInterval(rm, 1000);

